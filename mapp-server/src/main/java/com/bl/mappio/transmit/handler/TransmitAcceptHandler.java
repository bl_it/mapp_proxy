package com.bl.mappio.transmit.handler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 16:25
 * @describe 类描述:
 */
public class TransmitAcceptHandler implements Runnable {
    private ServerSocket serverSocket;
    private ExecutorService executorService = Executors.newCachedThreadPool();

    public TransmitAcceptHandler(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    @Override
    public void run() {
        while (!serverSocket.isClosed()){
            try {
                Socket client = serverSocket.accept();
                executorService.execute(new TransmitClientHandler(client));
            }catch (SocketException e){

            }catch(IOException e) {
                e.printStackTrace();
            }
        }
    }
}
