package com.bl.mappio;

import com.bl.common.utils.PropertiesUtil;
import com.bl.mappio.proxy.server.ProxyServer;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 9:00
 * @describe 类描述:
 */
public class ServerTest {
    public static void main(String[] args){
        int port = Integer.parseInt(PropertiesUtil.get("proxy.port").toString());

        ProxyServer proxyServer = new ProxyServer(port);
    }
}
