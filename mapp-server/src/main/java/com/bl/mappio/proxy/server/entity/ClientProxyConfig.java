package com.bl.mappio.proxy.server.entity;

import com.bl.common.entity.ProxyConfig;
import com.bl.nio.common.entity.SocketClient;

import lombok.Data;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 19:30
 * @describe 类描述:
 */
@Data
public class ClientProxyConfig {
    private ProxyConfig proxyConfig;

    private SocketClient client;
}
