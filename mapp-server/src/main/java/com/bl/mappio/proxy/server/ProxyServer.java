package com.bl.mappio.proxy.server;

import com.bl.common.entity.Message;
import com.bl.common.entity.ProxyConfig;
import com.bl.mappio.proxy.server.entity.ClientProxyConfig;
import com.bl.mappio.transmit.TransmitServer;
import com.bl.nio.common.pack.EventPack;
import com.bl.nio.server.NioServer;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 8:54
 * @describe 类描述:
 */

public class ProxyServer {
    private NioServer nioServer;

    public final static Map<String, List<ClientProxyConfig>> proxyMap = new ConcurrentHashMap<>();

    public final static Map<String,Object> cacheProxyMessages = new HashMap<>();


    public ProxyServer(int port){
        try {
            nioServer = new NioServer(port, new ProxyServerClientHandler());

            //80端口专门做域名转发的
            TransmitServer transmitServer = new TransmitServer(80);
            System.out.println("开启代理服务器成功,"+port);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static Message proxy(Integer remotePort,String content){
        //从content内容中拿到 host
        String pattern = "Host:\\s(.*)";
        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(content);

        String remote = remotePort+"";
        if (m.find()){
            remote = m.group(1);
        }

        if (!proxyMap.containsKey(remote)){  //判断是否有这个端口
            Message message = new Message();
            message.setPort(null);
            message.setRemote(remote);
            message.setTime(System.currentTimeMillis());
            message.setData("hello world!\nno client listener.");
            return message;
        }

        List<ClientProxyConfig> clientProxyConfigs = proxyMap.get(remote);
        //做一个随机负载
        Random random = new Random();
        int index = random.nextInt(clientProxyConfigs.size());
        ClientProxyConfig clientProxyConfig = clientProxyConfigs.get(index);
        ProxyConfig proxyConfig = clientProxyConfig.getProxyConfig();

        Message message = new Message();
        message.setPort(proxyConfig.getPort());
        message.setRemote(proxyConfig.getRemote());
        message.setTime(System.currentTimeMillis());
        message.setData(content);

        try {
            EventPack proxy = clientProxyConfig.getClient().sendEventPack("proxy", message);
            String uuid = proxy.getUuid();
            int times = 0;
            while (!cacheProxyMessages.containsKey(uuid)){
                Thread.sleep(50);
                times += 50;
                if (times > 3000){
                    System.out.println("超时未获取代理数据");
                    return null;
                }
            }
            Object o = cacheProxyMessages.get(uuid);
            Message message2 = (Message) cacheProxyMessages.get(uuid);
            return message2;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
