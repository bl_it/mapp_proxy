package com.bl.nio;

import com.bl.common.entity.ProxyConfig;
import com.bl.nio.client.NioClient;
import com.bl.nio.common.handler.EventHandlerImpl;
import com.bl.nio.common.pack.EventPack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 11:34
 * @describe 类描述:
 */
public class NioClientTest {
    public static void main(String[] args){
        try {
            NioClient nioClient = new NioClient("127.0.0.1", 1999, new EventHandlerImpl());

//            while (true){
//                Thread.sleep(1000);
//                nioClient.getClient().send("你好啊");
//            }

            Thread.sleep(1000);
            List<ProxyConfig> proxyConfigs = new ArrayList<>();
//            proxyConfigs.add(new ProxyConfig(80,2000));
            EventPack eventPack = new EventPack();
            eventPack.setData(proxyConfigs);
            nioClient.getClient().sendEventPack("test",eventPack);

//            nioClient.getClient().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
