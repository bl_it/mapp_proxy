package com.bl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/6 8:31
 * @describe 类描述:
 */
public class TestMain {
    private ServerSocket serverSocket;


    public static void main(String[] args){
        TestMain testMain = new TestMain();
        testMain.start();
    }

    public void start(){
        try {
            serverSocket = new ServerSocket(2000);
            new Thread(new AcceptHandler()).start();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("服务器启动失败");
        }
    }

    class AcceptHandler implements Runnable{

        @Override
        public void run() {
            while (!serverSocket.isClosed()){
                try {
                    Socket socket = serverSocket.accept();
                    System.out.println("用户进入");
                    new Handler(socket).run();
                    //new Thread(new Handler(socket)).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class Handler implements Runnable{
        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                InputStream inputStream = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                String message = "";
                //第一种方法
                /*int available = inputStream.available();
                byte[] bytes = new byte[available];
                inputStream.read(bytes);
                message = new String(bytes);*/
               // message = new String(StreamUtil.read(inputStream));



                System.out.println("收到消息，"+message);
                writer.write("HTTP/1.1 200 ok");
                writer.write("\r\n");
                writer.write("\r\n");
                writer.write("hello world!");

                writer.flush();
                writer.close();
                reader.close();
                socket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
