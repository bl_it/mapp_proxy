package com.bl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/6 10:12
 * @describe 类描述:
 */
public class LocalSocketTest {
    public static void main(String[] args){

        Socket socket2 = null;
        try {
            socket2 = new Socket("127.0.0.1",80);
            OutputStream outputStream = socket2.getOutputStream();

            String str = "GET / HTTP/1.1\r\n" +
                    "Connection: Keep-Alive\r\n" +
                    "Accept: */*\r\n" +
                    "Accept-Language: zh-cn\r\n" +
                    "Referer: http://localhost:2000/\r\n" +
                    "User-Agent: Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)\r\n" +
                    "Host: localhost:2000\n\n" ;

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            writer.write(str);
            writer.flush();
            InputStream inputStream = socket2.getInputStream();

            //String read = new String(StreamUtil.read(inputStream,true));
            //System.out.println(read);

            writer.close();
            inputStream.close();
            socket2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
