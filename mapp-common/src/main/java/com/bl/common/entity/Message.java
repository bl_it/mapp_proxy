package com.bl.common.entity;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/3 19:26
 * @describe 类描述:
 */
@Data
@ToString
public class Message implements Serializable {

    private Integer port;

    private String remote;

    private Object data;

    private Long time;

    public Message(Integer port, String remote, Object data) {
        this.port = port;
        this.remote = remote;
        this.data = data;
    }

    public Message() {

    }

    public Long getTime(){
        if (time == null){
            return System.currentTimeMillis();
        }
        return time;
    }
}
