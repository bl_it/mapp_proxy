package com.bl.common.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/4 8:59
 * @describe 类描述:
 */
@Data
public class ProxyConfig implements Serializable {
    private String host;

    //本地端口
    private int port;

    //远程端口/域名
    private String remote;


    public ProxyConfig(int port, String remote) {
        this.port = port;
        this.remote = remote;
    }

    public boolean isDomain(){
        return remote.contains(".");
    }
}
