package com.bl.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/6 8:59
 * @describe 类描述:
 */
public class StreamUtil {

    public static byte[] read(InputStream inputStream) throws IOException {
        return read(inputStream,false).get(0);
    }

    public static void wait(InputStream inputStream) throws IOException {
        int available = inputStream.available();

        int times = 0;
        if (available == 0){
            while (available == 0){
                times += 10;
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                available = inputStream.available();
                if (times > 5000){ //超时
                    return;
                }
            }
        }
    }

    public static void write(OutputStream outputStream,String content) throws IOException {
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
        bufferedOutputStream.write(content.getBytes());
        bufferedOutputStream.flush();
    }

    public static void write(OutputStream outputStream,List<byte[]> byteList) throws IOException {
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
        for (byte[] bytes : byteList) {
            bufferedOutputStream.write(bytes);
        }
        bufferedOutputStream.flush();
    }

    public static List<byte[]> read(InputStream inputStream,boolean isWait) throws IOException {
        if (isWait){
            wait(inputStream);
        }
        int available = 0;
        List<byte[]> byteList = new ArrayList<>();
        while ((available = inputStream.available())!=0){
            byte[] bytes = new byte[available];
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            bufferedInputStream.read(bytes);
            byteList.add(bytes);
        }

        return byteList;
    }


    public static String readS(InputStream inputStream,boolean isWait) throws IOException {
        if (isWait){
            wait(inputStream);
        }
        return readS(inputStream);
    }

    public static String readS(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        while (null != (line = reader.readLine())){
            stringBuilder.append(line + "\r\n");
        }
        return stringBuilder.toString();
    }

    public static <T>  T readObject(InputStream inputStream) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        return (T)objectInputStream.readObject();
    }

    public static void writeObject(OutputStream outputStream,Object object) throws IOException, ClassNotFoundException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(object);
        objectOutputStream.flush();
    }
}
