package com.bl.common.utils;

import cn.hutool.setting.dialect.Props;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/8 9:50
 * @describe 类描述:
 */

public class PropertiesUtil {
    private final static Props props = new Props("application.properties");


    public static Object get(String name){
        if (System.getProperty(name) != null){
            return System.getProperty(name);
        }
        return props.get(name);
    }
}
