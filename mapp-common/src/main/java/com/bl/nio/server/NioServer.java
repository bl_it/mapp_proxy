package com.bl.nio.server;

import com.bl.nio.common.entity.Transport;
import com.bl.nio.server.handler.AcceptHandler;
import com.bl.nio.common.handler.IEventHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 10:44
 * @describe 类描述:
 */
public class NioServer {
    private ExecutorService executorService = Executors.newCachedThreadPool();

    public ServerSocket serverSocket;

    public IEventHandler eventHandler;

    public NioServer(int port,IEventHandler eventHandler) throws IOException {
        Transport transport = new Transport(port);
        serverSocket = transport.newServerSocket();

        executorService.execute(new AcceptHandler(serverSocket,eventHandler));

    }
}
