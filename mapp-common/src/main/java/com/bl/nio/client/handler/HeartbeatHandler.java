package com.bl.nio.client.handler;

import com.bl.nio.common.entity.SocketClient;

import java.io.IOException;
import java.net.SocketException;
import java.util.TimerTask;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 14:53
 * @describe 类描述:
 */
public class HeartbeatHandler extends TimerTask {
    private SocketClient client;
    public HeartbeatHandler(SocketClient client) {
        this.client = client;
    }

    @Override
    public void run() {
        try {
            client.sendHeartbeatPack();
        } catch (SocketException e) {
            client.getTimer().cancel();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
