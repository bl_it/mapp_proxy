package com.bl.nio.common.handler;

import com.bl.nio.common.entity.SocketClient;
import com.bl.nio.common.pack.EventPack;
import com.bl.nio.common.pack.HeartbeatPack;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 11:05
 * @describe 类描述:
 */
public interface IEventHandler {
    /**
     * 连接事件
     * @param client
     */
    void onConnect(SocketClient client);

    /**
     * 断开连接事件
     * @param client
     */
    void onDisConnect(SocketClient client);

    /**
     * 收到心跳包
     * @param client
     */
    void onHeartbeatPack(SocketClient client, HeartbeatPack heartbeatPack);

    /**
     * 接收消息事件
     * @param client
     * @param pack
     */
    void onEventPack(SocketClient client, EventPack pack);

    void onMessage(SocketClient client, Object message);
}
