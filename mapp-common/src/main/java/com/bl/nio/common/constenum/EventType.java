package com.bl.nio.common.constenum;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 10:59
 * @describe 类描述:
 */
public enum EventType {
    EVENT_CONNECT,
    EVENT_DISCONNECT,
    EVENT_MESSAGE
}
