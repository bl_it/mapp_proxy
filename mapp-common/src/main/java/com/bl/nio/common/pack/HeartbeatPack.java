package com.bl.nio.common.pack;

import java.io.Serializable;

import lombok.Data;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 14:48
 * @describe 类描述:
 */
@Data
public class HeartbeatPack implements Serializable {
    private int status;
}
