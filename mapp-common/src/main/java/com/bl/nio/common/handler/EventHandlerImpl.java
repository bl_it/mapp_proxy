package com.bl.nio.common.handler;

import com.bl.nio.common.entity.SocketClient;
import com.bl.nio.common.pack.EventPack;
import com.bl.nio.common.pack.HeartbeatPack;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 11:12
 * @describe 类描述:
 */
public class EventHandlerImpl implements IEventHandler {
    @Override
    public void onConnect(SocketClient client) {
        System.out.println("onConnect");
    }

    @Override
    public void onDisConnect(SocketClient client) {
        System.out.println("onDisConnect");
    }

    @Override
    public void onHeartbeatPack(SocketClient client, HeartbeatPack heartbeatPack) {
        System.out.println("onHeartbeatPack");
    }

    @Override
    public void onEventPack(SocketClient client, EventPack pack) {
        System.out.println("onEventPack,"+pack);
    }

    @Override
    public void onMessage(SocketClient client, Object message) {
        System.out.println("onMessage,"+message);
    }


}
