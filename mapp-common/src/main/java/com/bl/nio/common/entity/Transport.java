package com.bl.nio.common.entity;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import lombok.Data;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 10:45
 * @describe 类描述:
 */
@Data
public class Transport {
    private String host;
    private int port;

    public Transport(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Transport(int port) {
        this.port = port;
    }

    public ServerSocket newServerSocket() throws IOException {
        return new ServerSocket(port);
    }

    public Socket newSocket() throws IOException {
        return new Socket(host,port);
    }
}
