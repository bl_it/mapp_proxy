package com.bl.nio.common.constant;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 15:11
 * @describe 类描述:
 */
public class Const {

    //客户端心跳时间
    public final static int CLIENT_HEARTBEAT_TIMES = 5 * 1000;

    //服务端心跳超时时间(3次未接收到客户端消息确认其掉线了)
    public final static int SERVER_HEARTBEAT_TIMEOUT_TIMES = 3 * CLIENT_HEARTBEAT_TIMES;


}
