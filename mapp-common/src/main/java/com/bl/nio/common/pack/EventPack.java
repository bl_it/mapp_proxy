package com.bl.nio.common.pack;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 15:16
 * @describe 类描述:
 */
@Data
@ToString
public class EventPack implements Serializable {
    private String uuid;
    private String name;
    private Object data;

}
