package com.bl.mappio;

import com.bl.common.entity.ProxyConfig;
import com.bl.common.utils.PropertiesUtil;
import com.bl.mappio.proxy.client.ProxyClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 9:01
 * @describe 类描述:
 */
public class ClientTest {
    public static void main(String[] args){

        List<ProxyConfig> proxyConfigs = new ArrayList<>();

        String ports = PropertiesUtil.get("proxy.client.ports").toString();
        String[] split1 = ports.split(",");
        for (String s : split1) {
            String[] split2 = s.split(":");
            if (split2.length != 2){
                System.out.println("代理端代理配置错误");
                return;
            }
            proxyConfigs.add(new ProxyConfig(Integer.parseInt(split2[0]),split2[1]));
        }



//        119.29.184.140
        String host = PropertiesUtil.get("proxy.server.host").toString();
        int port = Integer.parseInt(PropertiesUtil.get("proxy.server.port").toString());
        int num = Integer.parseInt(PropertiesUtil.get("proxy.client.num").toString());
        String token = PropertiesUtil.get("proxy.client.token").toString();
        ProxyClient proxyClient = new ProxyClient(host,port,proxyConfigs,num,token);

    }

    public byte[] serialize(Object data) {
        byte[] dataArray = null;
        try {
            //1、创建OutputStream对象
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            //2、创建OutputStream的包装对象ObjectOutputStream，PS：对象将写到OutputStream流中
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            //3、将对象写到OutputStream流中
            objectOutputStream.writeObject(data);
            //4、将OutputStream流转换成字节数组
            dataArray = outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataArray;
    }


    public Object deserialize(byte[] data) {
        Object object = null;

        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            object = objectInputStream.readObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return object;
    }
}
