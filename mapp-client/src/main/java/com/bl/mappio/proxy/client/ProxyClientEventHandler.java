package com.bl.mappio.proxy.client;

import com.bl.common.entity.Message;
import com.bl.common.utils.StreamUtil;
import com.bl.nio.common.entity.SocketClient;
import com.bl.nio.common.handler.IEventHandler;
import com.bl.nio.common.pack.EventPack;
import com.bl.nio.common.pack.HeartbeatPack;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;

/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 15:26
 * @describe 类描述:
 */
public class ProxyClientEventHandler implements IEventHandler {

    @Override
    public void onConnect(SocketClient client) {
        System.out.println("onConnect");
    }

    @Override
    public void onDisConnect(SocketClient client) {
        System.out.println("onDisConnect");
    }

    @Override
    public void onHeartbeatPack(SocketClient client, HeartbeatPack heartbeatPack) {
        System.out.println("onHeartbeatPack");
    }

    @Override
    public void onEventPack(SocketClient client, EventPack pack) {
        System.out.println("onEventPack,"+pack);
        if (pack.getName().equals("proxy")){
            onProxy(client,pack);
        }
        if (pack.getName().equals("proxyFail")){
            System.out.println(pack.getData().toString());
        }
    }

    private void onProxy(SocketClient client,EventPack pack){
        Message message = (Message)pack.getData();

        Object proxy = proxy(message);
        message.setData(proxy);
        System.out.println("代理客户端写入代理内容，"+message);
        pack.setData(message);
        try {
            client.sendEventPack(pack);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Object proxy(Message message){
        Socket socket = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            socket = new Socket("127.0.0.1",message.getPort());
            outputStream = socket.getOutputStream();

            String str = message.getData() + "\n\n";
            outputStream.write(str.getBytes());
            outputStream.flush();

            inputStream = socket.getInputStream();
            List<byte[]> bytes =  StreamUtil.read(inputStream, true);

            return bytes;
        } catch (IOException e) {
            System.out.println("无法连接本地端口："+message.getPort());
            return "无法连接本地端口："+message.getPort();
//            e.printStackTrace();
        } finally {
            if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onMessage(SocketClient client, Object message) {
//        System.out.println("onMessage,"+message);
    }
}
