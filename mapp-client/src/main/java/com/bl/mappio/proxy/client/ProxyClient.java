package com.bl.mappio.proxy.client;

import com.bl.common.entity.ProxyConfig;
import com.bl.nio.client.NioClient;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author 作者 : blownsow
 * @version 版本: 1.0
 * @date 创建时间 : 2019/12/7 8:54
 * @describe 类描述:
 */
public class ProxyClient {
    private NioClient nioClient;

    public ProxyClient(String host, int port, List<ProxyConfig> proxyConfigs){
        try {
            create(host,port,proxyConfigs,null);
            System.out.println("开启代理客户端成功");
        }catch (ConnectException e){
            System.out.println("连接代理服务端失败");
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ProxyClient(String host, int port, List<ProxyConfig> proxyConfigs,int num,String token){
        for (int i = 0; i < num; i++) {
            try {
                create(host,port,proxyConfigs,token);
            }catch (ConnectException e){
                System.out.println("连接代理服务端失败");
                return;
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("开启代理客户端成功");
    }

    public void create(String host, int port, List<ProxyConfig> proxyConfigs,String token) throws IOException {
        nioClient = new NioClient(host, port, new ProxyClientEventHandler());

        Map<String,Object> map = new HashMap<>();
        map.put("proxyConfigs",proxyConfigs);
        map.put("token",token);
        nioClient.getClient().sendEventPack("init",map);
    }
}
